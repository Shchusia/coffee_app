from api_mobile.postgres import decorator_db
from api_mobile.memcache.memdb import set_data_network_shop, \
    get_data_network_shop,\
    set_data_shop,\
    get_data_shop
from api_mobile.answer_server import good_get_shops,\
    good_get_cities,\
    good_get_products, \
    error_not_correct_data,\
    good_create_order,\
    good_create_order_with_update_quantity,\
    good_get_history,\
    error_update_not_new_order,\
    error_update_not_your_order,\
    good_remove_products_from_order,\
    good_reject_order,\
    good_get_favorite_shops,\
    good_get_favorite_products,\
    error_work_with_favorite_products,\
    good_add_to_favorite_products,\
    good_remove_from_favorite_products,\
    good_sign_in,\
    error_authorisation,\
    good_get_swap_products,\
    not_good_swap_product,\
    good_get_budget_coffee_shop,\
    good_create_comment_to_write_off,\
    good_move_barista,\
    error_move_barista
import random
import json
import jwt
from config_app import role_client,\
    secret_word,\
    algorithm_encode,\
    role_worker_network


def data_to_token(data):
    '''
    формирует токен по данным пользователя json
    :param data: данные пользователя имя пароль id
    :return: возвращает токен для пользователя
    '''

    return jwt.encode(data, secret_word, algorithm=algorithm_encode).decode()


def token_to_data(token):
    '''
    преобразовывает токен в данные пользователя
    :param token: токен который выдавался пользователю
    :return:json с данными пользователя
    '''
    try:
        return True, jwt.decode(token,
                                secret_word,
                                algorithms=[algorithm_encode])
    except jwt.exceptions.DecodeError:
        return False, {}


@decorator_db
def db_sign_in_barista(login,
                       password,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_worker, phone,password_hash, login, full_name,ref_id_coffee_shop_network,ref_id_coffee_shop
        FROM worker
        WHERE login = '{}'
        AND password_hash = '{}'
    '''.format(login, password)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[3] == login:
            if row[2] == password:
                data_user = {
                    'id_worker': row[0],
                    'phone': row[1],
                    'full_name': row[4],
                    'login': row[3],
                    'id_network': row[5],
                    'id_shop': row[6],
                    'role': role_worker_network
                }
                token = data_to_token(data_user)
                return {
                    'code': 1,
                    'message': good_sign_in,
                    'data': {
                        'user': data_user},
                    'token': token
                }
    return {
        'code': 1,
        'message': error_authorisation,
        'data': {}
    }


@decorator_db
def db_get_products(shop_id,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT pr.id_product, pr.ref_id_category,pr.title_product, pr.price, pr.path_img, c.title_category
        FROM product pr, category c
        WHERE pr.ref_id_coffee_shop = {}
        AND c.id_category = pr.ref_id_category
    '''.format(shop_id)
    cur.execute(select)
    rows = cur.fetchall()
    result = {}
    for r in rows:
        try:
            arr = result[r[5]]
            arr.append({
                'id_product': r[0],
                'title_product': r[2],
                'price': r[3],
                'path_img': r[4]
            })
            result[r[5]] = arr
        except KeyError:
            result[r[5]] = [
                {
                    'id_product': r[0],
                    'title_product': r[2],
                    'price': float(r[3]),
                    'path_img': r[4]
                }
            ]
    res = [
        {
            'name_category': r,
            'products': result[r]
        } for r in result.keys()
    ]

    return {
        'code': 1,
        'message': good_get_products,
        'data': {
            'categories': res
        }
    }


def check_product_in_one_shop(products,
                              id_shop,
                              cur):
    list_products = []
    for p in products:
        # print(p)
        list_products.append(str(p['id_product']))
        try:
            for t in p['toppings']:
                list_products.append(str(t))
        except KeyError:
            pass
    select = '''
            SELECT COUNT (ref_id_coffee_shop)
            FROM product
            WHERE id_product in ({})
            AND ref_id_coffee_shop = {}
        '''.format(','.join(list_products),
                   id_shop)
    cur.execute(select)
    count_id = cur.fetchone()[0]
    print(count_id == len(list_products))
    return count_id == len(list_products), list_products


@decorator_db
def db_get_product(id_product,
                   *args,
                   **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_product, title_product, path_img, description, price, quantity, sub_product, list_typing
        FROM product
        WHERE id_product = {}
    '''.format(id_product)
    cur.execute(select)
    row = cur.fetchone()
    data = {
        'id_product': row[0],
        'title_product': row[1],
        'path_img': row[2],
        'description': row[3],
        'price': float(row[4]),
        'quantity': row[5],
        'sub_product': row[6]
    }
    toppings = []
    if row[7]:
        for t in row[7]:
            toppings.append(db_get_product(t))
    data['toppings'] = toppings
    return data


def sub_quantity_product(id_product,
                         quantity,
                         cur):
    select = '''
        update product
        SET quantity = quantity - {}
        WHERE id_product = {}
    '''.format(quantity, id_product)
    cur.execute(select)


def db_create_order_barista(products,
                            id_shop,
                            *args,
                            **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    is_valid_data, list_products = check_product_in_one_shop(products,
                                                             id_shop,
                                                             cur)

    if is_valid_data:
        insert_into = '''
            INSERT INTO orders (ref_id_order_status, ref_id_coffee_shop) 
            VALUES (1,{}) RETURNING id_order 
        '''.format(id_shop)
        cur.execute(insert_into)
        id_order = cur.fetchone()[0]
        return insert_products_to_order_barista(id_order,
                                                products,
                                                list_products,
                                                cur,
                                                conn)

    return {
        'code': 1,
        'message': error_not_correct_data,
        'data': {}
    }


def insert_products_to_order_barista(id_order,
                                     products,
                                     list_products,
                                     cur,
                                     conn):
    to_pay = 0
    select_get_price = '''
                SELECT id_product, quantity, price,title_product, is_participates_in_gatherer
                FROM product
                WHERE id_product IN ({})
            '''.format(','.join(list_products))
    cur.execute(select_get_price)
    data_pr = {
        i[0]: {
            'quantity': i[1],
            'price': i[2],
            'title': i[3],
            'is_participates_in_gatherer': i[4]
        }
        for i in cur.fetchall()
    }
    res_data = []
    is_update_quantity_gl = False
    for p in products:
        tmp_pay = 0
        is_update_quantity = False
        get_quantity_product_to_order = p['quantity']
        if get_quantity_product_to_order > data_pr[p['id_product']]['quantity']:
            get_quantity_product_to_order = data_pr[p['id_product']]['quantity']
            is_update_quantity = True
            is_update_quantity_gl = True
        if get_quantity_product_to_order > 0:
            sub_quantity_product(p['id_product'],
                                 get_quantity_product_to_order,
                                 cur)
            tmp_pay += get_quantity_product_to_order * data_pr[p['id_product']]['price']
            res_data.append({
                'id_product': p['id_product'],
                'title': data_pr[p['id_product']]['title'],
                'quantity': get_quantity_product_to_order,
                'is_update_quantity': is_update_quantity,
                'price': float(data_pr[p['id_product']]['price'])

            })

            try:
                for toppi in p['toppings']:
                    is_update_quantity = False
                    get_quantity_product_to_order = p['quantity']
                    if get_quantity_product_to_order > data_pr[toppi]['quantity']:
                        get_quantity_product_to_order = data_pr[toppi]['quantity']
                        is_update_quantity = True
                        is_update_quantity_gl = True
                    sub_quantity_product(p['id_product'],
                                         get_quantity_product_to_order,
                                         cur)
                    tmp_pay += get_quantity_product_to_order * data_pr[toppi]['price']
                    res_data.append({
                        'id_product': p[toppi],
                        'title': data_pr[p[toppi]]['title'],
                        'quantity': get_quantity_product_to_order,
                        'is_update_quantity': is_update_quantity,
                        'price': float(data_pr[p['id_product']]['price'])

                    })
            except KeyError:
                pass
            to_pay += tmp_pay
            select_insert_order = '''
                        INSERT INTO order_product (id_order_product, price, title, other_information, quantity, ref_id_order) 
                        VALUES (DEFAULT, {}, '{}', '{}',{},{} )
                    '''.format(tmp_pay,
                               data_pr[p['id_product']]['title'],
                               json.dumps(p),
                               get_quantity_product_to_order,
                               id_order)
            # print(select_insert_order)
            cur.execute(select_insert_order)

    conn.commit()
    if is_update_quantity_gl:
        message = good_create_order_with_update_quantity
    else:
        message = good_create_order
    return {
        'code': 1,
        'message': message,
        'data': {
            'order': {
                'id_order': id_order,
                'to_pay': float(to_pay),
                'code': '000000',
                'products': res_data
            }
        }
    }


@decorator_db
def db_get_orders_shop(id_shop,
                       date_start,
                       limit,
                       offset,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_order, date_create, code_order, ref_id_order_status, ref_id_coffee_shop, status, reprieve_time
        FROM orders, order_status
        WHERE ref_id_coffee_shop = {}
        AND date_create <= '{}'
        AND ref_id_order_status = id_order_status
        ORDER BY id_order DESC, ref_id_order_status ASC 
        LIMIT {} OFFSET {}
    '''.format(id_shop,
               '{}-{}-{}'.format(date_start.year, date_start.month, date_start.day),
               int(limit),
               int(limit)*int(offset))
    # print(select)
    cur.execute(select)
    rows = cur.fetchall()
    data = [
        {
            'id_order': r[0],
            'date_create': str(r[1]),
            'code_order': r[2],
            'id_order_status': r[3],
            'order_status':r[5],
            'shop': get_information_shop(r[4],
                                         cur),
            'products': get_products_order(r[0],
                                           cur),
            'reprieve_time':r[6]
        }
        for r in rows
    ]
    for d in data:
        d['price'] = get_to_price(d['products'])
    return {
        'code': 1,
        'message': good_get_history,
        'data': {
            'orders': data
        }
    }


def get_products_order(id_order,
                       cur):
    select = '''
        SELECT id_order_product, price, quantity,title,other_information
        FROM order_product
        WHERE ref_id_order = {}
    '''.format(id_order)
    cur.execute(select)
    rows = cur.fetchall()
    res = []
    for row in rows:
        data = {
            'id_order_product': row[0],
            'id_product': row[4]['id_product'],
            'price': float(row[1]),
            'quantity': row[2],
            'title': row[3],

        }
        toppings = []
        for t in row[4]['toppings']:
            toppings.append(db_get_product(t))
        data['toppings'] = toppings
        res.append(data)
    return res


def get_to_price(products):
    res_sum = 0
    for p in products:
        res_sum += p['price']
    return float(res_sum)


def get_information_shop(id_shop,
                         cur):
    data = get_data_shop(id_shop)
    if data:
        return data
    else:
        select = '''
            SELECT id_coffee_shop, title, address, ref_id_coffee_shop_network, title_network
            FROM coffee_shop,coffee_shop_network
            WHERE id_coffee_shop = {}
            AND id_coffee_shop_network = ref_id_coffee_shop_network
        '''.format(id_shop)
        cur.execute(select)
        row = cur.fetchone()
        data = {
            'id_shop': row[0],
            'title': row[1],
            'address': row[2]
        }
        set_data_shop(id_shop, data)
        return data


@decorator_db
def get_list_products_to_swap(id_shop,
                              *args,
                              **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_product
        FROM product, coffee_shop, coffee_shop_network
        WHERE is_to_swap = TRUE 
        AND ref_id_coffee_shop = {}
        AND ref_id_coffee_shop = id_coffee_shop
        AND ref_id_coffee_shop_network = id_coffee_shop_network
        AND is_gatherer_shares = TRUE 
    '''.format(id_shop)
    cur.execute(select)
    answer = [
        db_get_product(r[0])
        for r in cur.fetchall()
    ]
    if answer:
        return {
            'code': 1,
            'message': good_get_swap_products,
            'data': {
                'products': answer
            }
        }
    return {
        'code': 1,
        'message': not_good_swap_product,
        'data': {}
    }


@decorator_db
def get_order(id_order,
              *args,
              **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
            SELECT id_order, date_create, code_order, ref_id_order_status, ref_id_coffee_shop, status, reprieve_time
            FROM orders, order_status
            WHERE id_order = {}
            AND ref_id_order_status = id_order_status
            ORDER BY id_order DESC, ref_id_order_status ASC 
        '''.format(id_order)
    # print(select)
    cur.execute(select)
    r = cur.fetcone()
    data ={
            'id_order': r[0],
            'date_create': str(r[1]),
            'code_order': r[2],
            'id_order_status': r[3],
            'order_status': r[5],
            'shop': get_information_shop(r[4],
                                         cur),
            'products': get_products_order(r[0],
                                           cur),
            'reprieve_time': r[6]

        }
    data['price'] = get_to_price(data['products'])
    return {
        'code': 1,
        'message': good_get_history,
        'data': {
            'order': data
        }
    }


@decorator_db
def update_status(id_order,
                  to_work,
                  id_shop,
                  *args,
                  **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    if to_work:
        code = 2
    else:
        code = 3
    select = '''
            UPDATE orders
            SET ref_id_order_status = {}
            WHERE id_order = {}
            AND ref_id_coffee_shop = {}
        '''.format(code, id_order, id_shop)
    cur.execute(select)
    conn.commit()
    return get_order(id_shop)


@decorator_db
def is_order_to_shop(id_shop,
                     id_order,
                     *args,
                     **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_order
        FROM orders
        WHERE ref_id_coffee_shop = {}
        AND id_order = {}
    '''.format(id_shop, id_order)
    cur.execute(select)
    if cur.fetchone():
        return True
    return False


@decorator_db
def get_comment_to_budget(id_shop,
                          date,
                          *args,
                          **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT write_off, description
        FROM comments_write_off
        WHERE ref_id_coffee_shop = {}
        AND DATE '{}' = DATE ("date")
    '''.format(id_shop,
               date)
    cur.execute(select)
    print(select)
    return [
        {
            'write_off': r[0],
            'comment': r[1]
        }
        for r in cur.fetchall()
    ]


@decorator_db
def get_budget_coffee_shop_day(id_shop,
                               date,
                               *args,
                               **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT SUM(op.price)
        FROM orders o, order_product op 
        WHERE o.ref_id_coffee_shop = {}
        AND o.id_order = op.ref_id_order
        AND DATE '{}' = DATE (o.date_create)       
    '''.format(id_shop,
               date,date,date)
    cur.execute(select)
    # print(select)
    budget = cur.fetchone()[0]
    if budget is None:
        budget = 0
    comments = get_comment_to_budget(id_shop,
                                     date)
    write_off = 0
    for c in comments:
        write_off += c['write_off']
    print(write_off)
    return {
        'code': 1,
        'message': good_get_budget_coffee_shop,
        'data': {
            'budget': float(budget-write_off),
            'comments': comments
        }
    }


@decorator_db
def db_write_off(id_shop,
                 comment,
                 write_off,
                 *args,
                 **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        INSERT INTO comments_write_off (id_comments_write_off, ref_id_coffee_shop, "date", write_off, description) 
        VALUES (DEFAULT, {}, DEFAULT, {}, '{}') 
    '''.format(id_shop,write_off, comment)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_create_comment_to_write_off,
        'data': {}
    }


@decorator_db
def db_get_shops_network(id_shop,
                         *args,
                         **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT c.id_coffee_shop, c.address, c.title,c.path_img
        FROM coffee_shop c
        WHERE c.ref_id_coffee_shop_network = (
                SELECT cs.ref_id_coffee_shop_network
                FROM coffee_shop cs
                WHERE cs.id_coffee_shop = {})      
        AND c.is_activate = TRUE
    '''.format(id_shop)
    cur.execute(select)
    data = [
        {
            'id_shop': r[0],
            'address': r[1],
            'title': r[2],
            'path_img': r[3]
        }for r in cur.fetchall()
    ]
    return {
        'code': 1,
        'message': good_get_shops,
        'data': {
            'shops': data
        }
    }


def db_is_barista_can_move_to_shop(id_shop,
                                   id_network,
                                   cur):
    select = '''
        SELECT cs.id_coffee_shop
        FROM coffee_shop cs
        WHERE cs.ref_id_coffee_shop_network = {}
        AND cs.id_coffee_shop = {}
    '''.format(id_network,
               id_shop)
    cur.execute(select)
    try:
        if cur.fetchone()[0] == id_shop:
            return True
        return False
    except:
        return False


@decorator_db
def db_move_barista(id_barista,
                    id_new_shop,
                    id_network,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    if db_is_barista_can_move_to_shop(id_new_shop,
                                      id_network,
                                      cur):
        select = '''
            UPDATE worker
            SET ref_id_coffee_shop = {}
            WHERE id_worker = {}
        '''.format(id_new_shop,
                   id_barista)
        cur.execute(select)
        conn.commit()

        return {
            'code': 1,
            'message': good_move_barista,
            'data': {}
        }
    return {
        'code': 1,
        'message': error_move_barista,
        'data': {}
    }

if __name__== '__main__':
    print(token_to_data('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9zaG9wIjo1LCJsb2dpbiI6ImRlbmlza2EiLCJpZF9uZXR3b3JrIjo2LCJyb2xlIjoid29ya2VyX25ldHdvcmsiLCJpZF93b3JrZXIiOjMsInBob25lIjoiIiwiZnVsbF9uYW1lIjoiREVuaXMifQ.3u3RIAeo_cgzIv48NE_Ir3ZEkJwjas2Ume7sCpO5cdM'))
