from flask import request, \
    jsonify
from api_mobile.__init__ import app,\
    check_valid_data_user,\
    check_valid_data_barista
from api_mobile.urls_barista import api_get_products_barista,\
    api_get_list_order, \
    api_create_order_barista,\
    api_entry_barista,\
    api_get_list_product_to_swap,\
    api_update_status_worker,\
    api_get_order,\
    api_get_budget_day, \
    api_move_barista,\
    api_withdraw_money,\
    api_get_list_shops_network
from api_mobile.answer_server import error_arguments,\
    good_sign_out,\
    error_not_valid_token,\
    good_get_shops, \
    good_get_product,\
    good_get_favorite_products,\
    good_get_favorite_shops,\
    error_order_not_your_shop,\
    error_not_date
from api_mobile.barista.db_barista import db_get_products,\
    db_get_product,\
    db_create_order_barista,\
    token_to_data,\
    db_sign_in_barista,\
    db_get_orders_shop,\
    get_list_products_to_swap,\
    update_status,\
    get_order,\
    is_order_to_shop,\
    get_budget_coffee_shop_day,\
    db_write_off,\
    db_get_shops_network,\
    db_move_barista
import datetime
import traceback
from api_mobile.redis.redb import set_client_multi,\
    sign_out_multi


def authorisation_barista():
    try:
        password = request.json['password']
        login = request.json['login']
        res = db_sign_in_barista(login,
                                 password)

        if res['message']['code'] > 0:
                token = res['token']
                del res['token']
                answer = jsonify(res)
                answer.headers.extend({
                    'Token': token
                })
                set_client_multi(res['data']['user']['id_worker'],
                                 token,
                                 res['data']['user']['role'])
                return answer
        return jsonify(res)
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })


@check_valid_data_barista
def get_list_products_barista():
    try:
        is_good, data = token_to_data(request.headers['Token'])
        return jsonify(db_get_products(data['id_shop']))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })


@check_valid_data_barista
def get_list_orders_barista():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        try:
            page = request.json['page']
        except KeyError:
            page = 0
        except TypeError:
            page = 0
        try:
            limit = request.json['limit']
        except KeyError:
            limit = 10
        except TypeError:
            limit = 10
        try:
            date = datetime.datetime(request.json['date'])
        except KeyError:
            date = datetime.datetime.now()
        except TypeError:
            date = datetime.datetime.now()
        return jsonify(db_get_orders_shop(data['id_shop'],
                                          date,
                                          limit,
                                          page))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })


@check_valid_data_barista
def create_order_barista():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        products = request.json['products']
        return jsonify(db_create_order_barista(products,
                                               data['id_shop']))
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_barista
def get_products_to_swap():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        return jsonify(get_list_products_to_swap(data['id_shop']))

    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_barista
def update_order_status():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_order = request.json['id_order']
        to_work = request.json['to_work']
        return jsonify(update_status(id_order,
                                     to_work,
                                     data['id_shop']))
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_barista
def get_order_barista(id_order):
    token = request.headers['Token']
    is_good, data = token_to_data(token)
    if is_order_to_shop(data['id_shop'], id_order):
        return jsonify(get_order(id_order))
    return jsonify(
        {
            'code': 1,
            'message': error_order_not_your_shop,
            'data': {}
        }
    )


@check_valid_data_barista
def get_budget_day():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        date = datetime.datetime.strptime(request.json['date'], '%d-%m-%Y')
        return jsonify(get_budget_coffee_shop_day(data['id_shop'],
                                                  date))
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )
    except ValueError:
        return jsonify({
            'code': 1,
            'message': error_not_date,
            'data': {}
        })


@check_valid_data_barista
def write_off():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        comment = request.json['comment']
        write_off = request.json['write_off']

        return jsonify(db_write_off(data['id_shop'],
                                    comment,
                                    write_off))
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )
    except ValueError:
        return jsonify({
            'code': 1,
            'message': error_not_date,
            'data': {}
        })


@check_valid_data_barista
def list_shops():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        return jsonify(db_get_shops_network(data['id_shop']))
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_barista
def move():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_new_shop = request.json['id_new_shop']
        return jsonify(db_move_barista(data['id_worker'],
                                       id_new_shop,
                                       data['id_network']))
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


app.add_url_rule(api_get_products_barista,
                 'get_list_products_barista',
                 get_list_products_barista,
                 methods=['GET'])
app.add_url_rule(api_get_list_order,
                 'get_list_orders_barista',
                 get_list_orders_barista,
                 methods=['POST'])
app.add_url_rule(api_create_order_barista,
                 'create_order_barista',
                 create_order_barista,
                 methods=['POST'])
app.add_url_rule(api_entry_barista,
                 'authorisation_barista',
                 authorisation_barista,
                 methods=['POST'])
app.add_url_rule(api_get_list_product_to_swap,
                 'get_products_to_swap',
                 get_products_to_swap,
                 methods=['GET'])
app.add_url_rule(api_update_status_worker,
                 'update_or',
                 update_order_status,
                 methods=['POST'])
app.add_url_rule(api_get_order,
                 'get_order',
                 get_order_barista,
                 methods=['GET'])
app.add_url_rule(api_get_budget_day,
                 'get_budget_day',
                 get_budget_day,
                 methods=['POST'])
app.add_url_rule(api_withdraw_money,
                 'write_off',
                 write_off,
                 methods=['POST'])
app.add_url_rule(api_move_barista,
                 'move_barista',
                 move,
                 methods=['POST'])
app.add_url_rule(api_get_list_shops_network,
                 'list_shops',
                 list_shops,
                 methods=['GET'])
