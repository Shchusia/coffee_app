from flask import request, \
    jsonify
from api_mobile.__init__ import app,\
    check_valid_data_user
from api_mobile.urls import api_get_user_info,\
    api_update_user
from api_mobile.answer_server import error_arguments
from api_mobile.entry.db_entry import token_to_data
from api_mobile.redis.redb import check_valid_data_multi
import datetime
import traceback
from api_mobile.user.db_user import db_user


def user():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        return jsonify(db_user(data['id_user']))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })



def user_update():
    pass


app.add_url_rule(api_get_user_info, 'user', user, methods=['GET'])
app.add_url_rule(api_update_user, 'user_update', user_update, methods=['POST'])
