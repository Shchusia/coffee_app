from api_mobile.postgres import decorator_db
from api_mobile.memcache.memdb import set_data_network_shop, \
    get_data_network_shop
from api_mobile.answer_server import good_get_user
from config_app import domain_photo, str_path_to_file


def get_data_social():
    pass


@decorator_db
def get_data_network(id_network,
                     *args,
                     **kwargs):
    data_network = get_data_network_shop(id_network)
    if data_network:
        return data_network
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_coffee_shop_network, is_ban, title_network, color_hash,quantity_gatherer,is_gatherer_shares
        FROM coffee_shop_network
        WHERE id_coffee_shop_network = {}
    '''.format(id_network)
    cur.execute(select)
    row = cur.fetchone()
    data = {
        'id_network': row[0],
        'is_ban': row[1],
        'title_network': row[2],
        'color_hash': row[3],
        'is_gatherer_shares': row[5],
        'quantity_gatherer': row[4]
    }
    set_data_network_shop(id_network, data)
    return data


def get_bonuses_user(id_user,
                     cur):
    select = '''
        SELECT ref_id_network, sum("count")
        FROM bonus_for_coffee
        WHERE ref_id_user = {}
        GROUP BY ref_id_network
    '''.format(id_user)
    cur.execute(select)
    rows = cur.fetchall()
    data_result = []
    for r in rows:
        data = get_data_network(r[0])
        data['bonuses'] = r[1]
        data_result.append(data)
    return data_result


@decorator_db
def db_user(id_user,
            *args,
            **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_user, full_name, phone, mail, is_social, path_img
        FROM users
        WHERE id_user = {}
    '''.format(id_user)
    cur.execute(select)
    user = cur.fetchone()
    social = {}
    if user[4]:
        # тут получать данные про социалку
        pass
    path_img = user[5]
    if not path_img:
        path_img = domain_photo + str_path_to_file + 'default.jpg'
    data = {
        'id_user': user[0],
        'full_name': user[1],
        'phone': user[2],
        'mail': user[3],
        'is_social': user[4],
        'path_img': path_img,
        'bonuses': get_bonuses_user(id_user, cur)
    }
    return {
        'code': 1,
        'message': good_get_user,
        'data': {
            'user': data
        }
    }
