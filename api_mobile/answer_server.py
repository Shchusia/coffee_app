good_registration = {
    'title': 'успех',
    'body': 'вы успешно зарегистрировались',
    'code': 1
}

good_sign_in = {
    'title': 'успех',
    'body': 'вы успешно вошли',
    'code': 2
}

good_sign_out = {
    'title': 'успех',
    'body': 'вы успешно вышли из учетной записи',
    'code': 3
}

good_get_shops = {
    'title': 'успех',
    'body': 'вы успешно получили кофейни',
    'code': 4
}

good_get_cities = {
    'title': 'успех',
    'body': 'вы успешно получили города',
    'code': 5
}

good_get_products = {
    'title': 'успех',
    'body': 'вы успешно получили товары',
    'code': 6
}

good_get_product = {
    'title': 'успех',
    'body': 'вы успешно получили товар',
    'code': 7
}

good_create_order = {
    'title': 'успех',
    'body': 'вы успешно создали заказ',
    'code': 8
}

good_create_order_with_update_quantity = {
    'title': 'успех',
    'body': 'вы успешно создали заказ, но в некоторых товарах было изменено колличество',
    'code': 9
}

good_get_history = {
    'title': 'успех',
    'body': 'вы успешно получили историю',
    'code': 10
}

good_remove_products_from_order = {
    'title': 'успех',
    'body': 'вы успешно удалили товары из заказа',
    'code': 11
}

good_reject_order = {
    'title': 'успех',
    'body': 'вы успешно отклонили заказ',
    'code': 12
}

good_get_favorite_shops = {
    'title': 'успех',
    'body': 'вы успешно получили избранные заведения',
    'code': 13
}

good_get_favorite_products = {
    'title': 'успех',
    'body': 'вы успешно получили избранные товары',
    'code': 14
}

good_add_to_favorite_products = {
    'title': 'успех',
    'body': 'вы успешно добавили в избранное',
    'code': 15
}

good_remove_from_favorite_products = {
    'title': 'успех',
    'body': 'вы успешно удалили из избранного ',
    'code': 16
}

good_get_user = {
    'title': 'успех',
    'body': 'вы успешно получили данные пользователя',
    'code': 17
}

good_get_swap_products = {
    'title': 'успех',
    'body': 'вы успешно получили список обменных товаров',
    'code': 18
}


good_get_budget_coffee_shop = {
    'title': 'успех',
    'body': 'вы успешно получили бюджет кофейни',
    'code': 19
}

good_create_comment_to_write_off = {
    'title': 'успех',
    'body': 'добавленн коммент к списанию',
    'code': 20
}

good_move_barista = {
    'title': 'успех',
    'body': 'вы успешно перевелись',
    'code': 21
}

error_arguments = {
    'title': 'ошибка',
    'body': 'недостаточно аргументов для выполнения запроса',
    'code': -1
}

error_unique_data = {
    'title': 'ошибка',
    'body': 'данные которые вы хотите использовать уже кем-то заняты',
    'code': -2
}

error_authorisation = {
    'title': 'ошибка',
    'body': 'неверный логин и/или пароль',
    'code': -3
}

error_not_valid_token = {
    'title': 'ошибка',
    'body': 'не валидный токен',
    'code': -4
}

error_not_correct_data = {
    'title': 'ошибка',
    'body': 'некорректные данные',
    'code': -5
}

error_update_not_new_order = {
    'title': 'ошибка',
    'body': 'вы не можете редактировать этот заказа так как он уже не является новым(в работе или завершен)',
    'code': -6
}

error_update_not_your_order = {
    'title': 'ошибка',
    'body': 'вы не можее редактировать данный заказ так как он принадлежит не вам',
    'code': -7
}

error_work_with_favorite_products = {
    'title': 'ошибка',
    'body': 'какая - то ошибка при работе с избраными товарами',
    'code': -8
}

not_good_swap_product = {
    'title': 'хз',
    'body': 'в этом заведении нет товаров для обмена или нет акции',
    'code': -9
}

error_order_not_your_shop = {
    'title': 'ошибка',
    'body': 'этот заказ не относится к вашему заведению или не существует вовсе',
    'code': -10
}

error_not_date = {
    'title': 'ошибка',
    'body': 'не формат даты %d-%m-%Y',
    'code': -11
}

error_move_barista = {
    'title': 'ошибка',
    'body': 'бариста не может быть перемещен',
    'code': -12
}