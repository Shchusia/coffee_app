from flask import request, \
    jsonify
from api_mobile.__init__ import app,\
    check_valid_data_user,\
    check_valid_data_barista
from api_mobile.urls import api_get_coffee_shops_by_coordinates,\
    api_get_coffee_shops_by_city,\
    api_get_city_list,\
    api_get_products,\
    api_get_product,\
    api_create_order,\
    api_list_orders,\
    api_update_order,\
    api_order_reject,\
    api_get_favorite_products,\
    api_get_favorite_shops,\
    api_favorite_update_product,\
    api_favorite_update_shop
from api_mobile.answer_server import error_arguments,\
    good_sign_out,\
    error_not_valid_token,\
    good_get_shops, \
    good_get_product,\
    good_get_favorite_products,\
    good_get_favorite_shops
from api_mobile.entry.db_entry import db_registration,\
    db_sign_in,\
    token_to_data
from api_mobile.redis.redb import check_valid_data_multi
from api_mobile.shops.db_shops import db_get_list_shops_range_by_distance,\
    db_get_cities,\
    db_get_list_shops_range_by_city_id,\
    db_get_products,\
    db_get_product,\
    db_create_order, \
    get_orders_user,\
    db_update_order,\
    db_reject_order,\
    db_get_favorite_shops,\
    db_get_favorite_products,\
    db_to_favorite_product,\
    db_to_favorite_shop
import datetime
import traceback
from api_mobile.urls_barista import api_get_products_barista,\
    api_get_list_order, \
    api_create_order_barista


def get_shops_by_location():
    try:
        lat = request.json['lat']
        lng = request.json['lng']
        try:
            page = request.json['page']
        except KeyError:
            page = 0
        try:
            limit = request.json['limit']
        except KeyError:
            limit = 10
        return jsonify(db_get_list_shops_range_by_distance(lat,
                                                           lng,
                                                           page,
                                                           limit))

    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })


def get_shops_by_city():
    try:
        id_city = request.json['id_city']
        try:
            page = request.json['page']
        except KeyError:
            page = 0
        try:
            limit = request.json['limit']
        except KeyError:
            limit = 10
        return jsonify(db_get_list_shops_range_by_city_id(id_city,
                                                          page,
                                                          limit))

    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })


def get_cities():
    return jsonify(db_get_cities())


def get_products(id_shop):
    return jsonify(db_get_products(id_shop))


def get_product(id_product):
    try:
        data = db_get_product(id_product)
        return jsonify({
            'code': 1,
            'message': good_get_product,
            'data': {
                'product': data
            }
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })


@check_valid_data_user
def create_order():
    try:

        # from pprint import pprint
        # pprint(request.json)
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        try:
            time = request.json['time']
        except KeyError:
            time = 0
        try:
            comment = request.json['comment']
        except KeyError:
            comment = ''
        products = request.json['products']
        id_shop = request.json['id_shop']
        return jsonify(db_create_order(data['id_user'],
                                       products,
                                       time,
                                       comment,
                                       id_shop))
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_user
def get_list_orders():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        try:
            page = request.json['page']
        except KeyError:
            page = 0
        except TypeError:
            page = 0
        try:
            limit = request.json['limit']
        except KeyError:
            limit = 10
        except TypeError:
            limit = 10
        try:
            date = datetime.datetime(request.json['date'])
        except KeyError:
            date = datetime.datetime.now()
        except TypeError:
            date = datetime.datetime.now()
        if is_good:
            return jsonify(get_orders_user(data['id_user'],
                                           date,
                                           limit,
                                           page))
        return jsonify({
            'code': -1,
            'message': error_not_valid_token,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })


@check_valid_data_user
def update_order():
    try:

        # from pprint import pprint
        # pprint(request.json)
        token = request.headers['Token']
        id_order = request.json['id_order']
        is_good, data = token_to_data(token)
        try:
            time = request.json['time']
        except KeyError:
            time = 0
        try:
            comment = request.json['comment']
        except KeyError:
            comment = ''
        products = request.json['products']
        id_shop = request.json['id_shop']
        return jsonify(db_update_order(id_order,
                                       id_shop,
                                       data['id_user'],
                                       products,
                                       time,
                                       comment))
    except KeyError:
        traceback.print_exc()

        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_user
def reject_order():
    try:
        token = request.headers['Token']
        id_order = request.json['id_order']
        is_good, data = token_to_data(token)
        return jsonify(db_reject_order(id_order,
                                       data['id_user']))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_user
def get_favorite_shops():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        try:
            page = request.json['page']
        except KeyError:
            page = 0
        except TypeError:
            page = 0
        try:
            limit = request.json['limit']
        except KeyError:
            limit = 10
        except TypeError:
            limit = 10
        return jsonify(db_get_favorite_shops(data['id_user'],
                                             page,
                                             limit))

    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_user
def get_favorite_products():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        try:
            page = request.json['page']
        except KeyError:
            page = 0
        except TypeError:
            page = 0
        try:
            limit = request.json['limit']
        except KeyError:
            limit = 10
        except TypeError:
            limit = 10
        return jsonify(db_get_favorite_products(data['id_user'],
                                                page,
                                                limit))

    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_user
def update_favorite_product():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_product = request.json['id_product']
        to_favorite = str(request.json['to_favorite']).lower()
        if to_favorite == 'true':
            to_favorite = True
        else:
            to_favorite = False

        return jsonify(db_to_favorite_product(data['id_user'],
                                              id_product,
                                              to_favorite))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )


@check_valid_data_user
def update_favorite_shop():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_shop = request.json['id_shop']
        to_favorite = str(request.json['to_favorite']).lower()
        if to_favorite == 'true':
            to_favorite = True
        else:
            to_favorite = False

        return jsonify(db_to_favorite_shop(data['id_user'],
                                           id_shop,
                                           to_favorite))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}}
        )





app.add_url_rule(api_get_coffee_shops_by_coordinates,
                 'get_shops_by_location',
                 get_shops_by_location,
                 methods=['POST'])
app.add_url_rule(api_get_coffee_shops_by_city,
                 'get_shops_by_city',
                 get_shops_by_city,
                 methods=['POST'])
app.add_url_rule(api_get_city_list,
                 'get_city',
                 get_cities,
                 methods=['GET'])
app.add_url_rule(api_get_products,
                 'get_products',
                 get_products,
                 methods=['GET'])
app.add_url_rule(api_get_product,
                 'get_product',
                 get_product,
                 methods=['GET'])
app.add_url_rule(api_create_order,
                 'create_order',
                 create_order,
                 methods=['POST'])
app.add_url_rule(api_list_orders,
                 'get_history',
                 get_list_orders,
                 methods=['POST'])
app.add_url_rule(api_update_order,
                 'update_order',
                 update_order,
                 methods=['POST'])
app.add_url_rule(api_order_reject,
                 'reject_order',
                 reject_order,
                 methods=['POST'])
app.add_url_rule(api_get_favorite_shops,
                 'favorite_shops',
                 get_favorite_shops,
                 methods=['POST'])
app.add_url_rule(api_get_favorite_products,
                 'get_favorite_products',
                 get_favorite_products,
                 methods=['POST'])
app.add_url_rule(api_favorite_update_shop,
                 'update_favorite_shop',
                 update_favorite_shop,
                 methods=['POST'])
app.add_url_rule(api_favorite_update_product,
                 'update_favorite_product',
                 update_favorite_product,
                 methods=['POST'])

