from api_mobile.postgres import decorator_db
from api_mobile.memcache.memdb import set_data_network_shop, \
    get_data_network_shop,\
    set_data_shop,\
    get_data_shop
from api_mobile.answer_server import good_get_shops,\
    good_get_cities,\
    good_get_products, \
    error_not_correct_data,\
    good_create_order,\
    good_create_order_with_update_quantity,\
    good_get_history,\
    error_update_not_new_order,\
    error_update_not_your_order,\
    good_remove_products_from_order,\
    good_reject_order,\
    good_get_favorite_shops,\
    good_get_favorite_products,\
    error_work_with_favorite_products,\
    good_add_to_favorite_products,\
    good_remove_from_favorite_products
import random
import json


@decorator_db
def get_data_network(id_network,
                     *args,
                     **kwargs):
    data_network = get_data_network_shop(id_network)
    if data_network:
        return data_network
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_coffee_shop_network, is_ban, title_network, color_hash
        FROM coffee_shop_network
        WHERE id_coffee_shop_network = {}
    '''.format(id_network)
    cur.execute(select)
    row = cur.fetchone()
    data = {
        'id_network': row[0],
        'is_ban': row[1],
        'title_network': row[2],
        'color_hash': row[3]
    }
    set_data_network_shop(id_network, data)
    return data


@decorator_db
def db_get_list_shops_range_by_distance(lat,
                                        lng,
                                        page,
                                        limit,
                                        *args,
                                        **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    sel = '''
        SELECT id_coffee_shop, ref_id_coffee_shop_network,title,path_img,address,ST_x(ST_Transform( location, 4326)),
          ST_y(ST_Transform( location, 4326)),
          ST_Distance(location::geography, 'SRID=4326;POINT({} {})'::geography)/1000 as dist_km
        FROM coffee_shop
        WHERE is_activate = TRUE 
        ORDER BY location <-> 'SRID=4326;POINT({} {})' limit {} OFFSET {};
    '''.format(lat, lng,
               lat, lng,
               limit,
               int(page) * int(limit))
    cur.execute(sel)
    rows = cur.fetchall()
    print(rows)
    result = [
        {
            'id_coffee_shop': r[0],
            'data_network': get_data_network(r[1]),
            'title': r[2],
            'path_img': r[3],
            'address': r[4],
            'location':{
                'lat': r[5],
                'lng':r[6]
            },
            'distance': r[7]
        } for r in rows if not bool(get_data_network(r[1])['is_ban'])]

    return {
        'code': 1,
        'message':good_get_shops,
        'data': {
            'shops': result
        }
    }


@decorator_db
def db_get_list_shops_range_by_city_id(id_city,
                                       page,
                                       limit,
                                       *args,
                                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    sel = '''
        SELECT id_coffee_shop, ref_id_coffee_shop_network,title,path_img,address,ST_x(ST_Transform( location, 4326)),
          ST_y(ST_Transform( location, 4326))
        FROM coffee_shop
        WHERE is_activate = TRUE 
        AND ref_id_city = {}
        ORDER BY id_coffee_shop ASC limit {} OFFSET {};
    '''.format(id_city,
               limit,
               int(page) * int(limit))
    cur.execute(sel)
    rows = cur.fetchall()
    # print(rows)
    result = [
        {
            'id_coffee_shop': r[0],
            'data_network': get_data_network(r[1]),
            'title': r[2],
            'path_img': r[3],
            'address': r[4],
            'location':{
                'lat': r[5],
                'lng':r[6]
            },
            'distance': 0
        } for r in rows if not bool(get_data_network(r[1])['is_ban'])]

    return {
        'code': 1,
        'message': good_get_shops,
        'data': {
            'shops': result
        }
    }


@decorator_db
def db_get_cities(*args,
                  **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_city, title_city
        FROM city
    '''
    cur.execute(select)
    res = [{
        'id_city': r[0],
        'city': r[1]
    }for r in cur.fetchall()]
    return {
        'code': 1,
        'message': good_get_cities,
        'data': {
            'cities': res
        }
    }


@decorator_db
def db_get_products(shop_id,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT pr.id_product, pr.ref_id_category,pr.title_product, pr.price, pr.path_img, c.title_category
        FROM product pr, category c
        WHERE pr.ref_id_coffee_shop = {}
        AND c.id_category = pr.ref_id_category
    '''.format(shop_id)
    cur.execute(select)
    rows = cur.fetchall()
    result = {}
    for r in rows:
        try:
            arr = result[r[5]]
            arr.append({
                'id_product': r[0],
                'title_product': r[2],
                'price': r[3],
                'path_img': r[4]
            })
            result[r[5]] = arr
        except KeyError:
            result[r[5]] = [
                {
                    'id_product': r[0],
                    'title_product': r[2],
                    'price': float(r[3]),
                    'path_img': r[4]
                }
            ]
    res = [
        {
            'name_category': r,
            'products': result[r]
        } for r in result.keys()
    ]

    return {
        'code': 1,
        'message': good_get_products,
        'data': {
            'categories': res
        }
    }


@decorator_db
def db_get_product(id_product,
                   *args,
                   **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_product, title_product, path_img, description, price, quantity, sub_product, list_typing
        FROM product
        WHERE id_product = {}
    '''.format(id_product)
    cur.execute(select)
    row = cur.fetchone()
    data = {
        'id_product': row[0],
        'title_product': row[1],
        'path_img': row[2],
        'description': row[3],
        'price': float(row[4]),
        'quantity': row[5],
        'sub_product': row[6]
    }
    toppings = []
    if row[7]:
        for t in row[7]:
            toppings.append(db_get_product(t))
    data['toppings'] = toppings
    return data


def sub_quantity_product(id_product,
                         quantity,
                         cur):
    select = '''
        update product
        SET quantity = quantity - {}
        WHERE id_product = {}
    '''.format(quantity, id_product)
    cur.execute(select)


@decorator_db
def to_favorite(id_user,
                products,
                id_shop,
                *args,
                **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    for p in products:
        try:
            insert = '''
                INSERT INTO favorite_product (ref_id_user, ref_id_product) 
                VALUES ({}, {})
            '''.format(id_user, p)
            cur.execute(insert)
            conn.commit()
        except:
            conn.rollback()
    try:
        insert_to_favorite_shop = '''
            INSERT INTO favorite_shop (ref_id_user, ref_id_shop) VALUES ({}, {})
        '''.format(id_user, id_shop)
        cur.execute(insert_to_favorite_shop)
        conn.commit()
    except:
        conn.rollback()


def check_product_in_one_shop(products,
                              id_shop,
                              cur):
    list_products = []
    for p in products:
        # print(p)
        list_products.append(str(p['id_product']))
        try:
            for t in p['toppings']:
                list_products.append(str(t))
        except KeyError:
            pass
    select = '''
            SELECT COUNT (ref_id_coffee_shop)
            FROM product
            WHERE id_product in ({})
            AND ref_id_coffee_shop = {}
        '''.format(','.join(list_products),
                   id_shop)
    cur.execute(select)
    count_id = cur.fetchone()[0]
    print(count_id == len(list_products))
    return count_id == len(list_products), list_products


def insert_products_to_order(id_order,
                             products,
                             list_products,
                             id_user,
                             id_shop,
                             code,
                             cur,
                             conn):
    to_pay = 0
    select_get_price = '''
                SELECT id_product, quantity, price,title_product, is_participates_in_gatherer
                FROM product
                WHERE id_product IN ({})
            '''.format(','.join(list_products))
    cur.execute(select_get_price)
    data_pr = {
        i[0]: {
            'quantity': i[1],
            'price': i[2],
            'title': i[3],
            'is_participates_in_gatherer': i[4]
        }
        for i in cur.fetchall()
    }
    res_data = []
    is_update_quantity_gl = False
    for p in products:
        tmp_pay = 0
        is_update_quantity = False
        get_quantity_product_to_order = p['quantity']
        if get_quantity_product_to_order > data_pr[p['id_product']]['quantity']:
            get_quantity_product_to_order = data_pr[p['id_product']]['quantity']
            is_update_quantity = True
            is_update_quantity_gl = True
        if get_quantity_product_to_order > 0:
            sub_quantity_product(p['id_product'],
                                 get_quantity_product_to_order,
                                 cur)
            tmp_pay += get_quantity_product_to_order * data_pr[p['id_product']]['price']
            res_data.append({
                'id_product': p['id_product'],
                'title': data_pr[p['id_product']]['title'],
                'quantity': get_quantity_product_to_order,
                'is_update_quantity': is_update_quantity,
                'price': float(data_pr[p['id_product']]['price'])

            })

            try:
                for toppi in p['toppings']:
                    is_update_quantity = False
                    get_quantity_product_to_order = p['quantity']
                    if get_quantity_product_to_order > data_pr[toppi]['quantity']:
                        get_quantity_product_to_order = data_pr[toppi]['quantity']
                        is_update_quantity = True
                        is_update_quantity_gl = True
                    sub_quantity_product(p['id_product'],
                                         get_quantity_product_to_order,
                                         cur)
                    tmp_pay += get_quantity_product_to_order * data_pr[toppi]['price']
                    res_data.append({
                        'id_product': p[toppi],
                        'title': data_pr[p[toppi]]['title'],
                        'quantity': get_quantity_product_to_order,
                        'is_update_quantity': is_update_quantity,
                        'price': float(data_pr[p['id_product']]['price'])

                    })
            except KeyError:
                pass
            to_pay += tmp_pay
            select_insert_order = '''
                        INSERT INTO order_product (id_order_product, price, title, other_information, quantity, ref_id_order) 
                        VALUES (DEFAULT, {}, '{}', '{}',{},{} )  returning id_order_product
                    '''.format(tmp_pay,
                               data_pr[p['id_product']]['title'],
                               json.dumps(p),
                               get_quantity_product_to_order,
                               id_order)
            # print(select_insert_order)
            cur.execute(select_insert_order)
            row = cur.fetchone()[0]

            if data_pr[p['id_product']]['is_participates_in_gatherer']:
                select_b = '''
                    INSERT INTO bonus_for_coffee (ref_id_product_order, ref_id_user, "count",ref_id_shop) VALUES ({}, {}, {})
                '''.format(row,
                           id_user,
                           get_quantity_product_to_order,
                           id_shop)
                cur.execute(select_b)
    conn.commit()
    if is_update_quantity_gl:
        message = good_create_order_with_update_quantity
    else:
        message = good_create_order
    to_favorite(id_user,
                list_products,
                id_shop)
    return {
        'code': 1,
        'message': message,
        'data': {
            'order': {
                'id_order': id_order,
                'to_pay': float(to_pay),
                'code': code,
                'products': res_data
            }
        }
    }


@decorator_db
def db_create_order(id_user,
                    products,
                    time,
                    comments,
                    id_shop,
                    *args,
                    **kwargs):
    def generate_code():
        return ''.join([str(random.randint(0, 9)) for _ in range(0, 6)])

    conn = kwargs['conn']
    cur = conn.cursor()
    is_valid_data, list_products = check_product_in_one_shop(products,
                                                             id_shop,
                                                             cur)

    if is_valid_data:
        code = generate_code()
        insert_into = '''
            INSERT INTO orders (code_order, ref_id_user, reprieve_time, comment, ref_id_order_status, ref_id_coffee_shop) 
            VALUES ('{}', '{}', {}, '{}', 1,{}) RETURNING id_order 
        '''.format(code,
                   id_user,
                   time,
                   comments,
                   id_shop)
        cur.execute(insert_into)
        id_order = cur.fetchone()[0]
        return insert_products_to_order(id_order,
                                        products,
                                        list_products,
                                        id_user,
                                        id_shop,
                                        code,
                                        cur,
                                        conn)

    return {
        'code': 1,
        'message': error_not_correct_data,
        'data': {}
    }


def get_information_shop(id_shop,
                         cur):
    data = get_data_shop(id_shop)
    if data:
        return data
    else:
        select = '''
            SELECT id_coffee_shop, title, address, ref_id_coffee_shop_network, title_network
            FROM coffee_shop,coffee_shop_network
            WHERE id_coffee_shop = {}
            AND id_coffee_shop_network = ref_id_coffee_shop_network
        '''.format(id_shop)
        cur.execute(select)
        row = cur.fetchone()
        data = {
            'id_shop': row[0],
            'title': row[1],
            'address': row[2]
        }
        set_data_shop(id_shop, data)
        return data


def get_products_order(id_order,
                       cur):
    select = '''
        SELECT id_order_product, price, quantity,title,other_information
        FROM order_product
        WHERE ref_id_order = {}
    '''.format(id_order)
    cur.execute(select)
    rows = cur.fetchall()
    res = []
    for row in rows:
        data = {
            'id_order_product': row[0],
            'id_product': row[4]['id_product'],
            'price': float(row[1]),
            'quantity': row[2],
            'title': row[3],

        }
        toppings = []
        for t in row[4]['toppings']:
            toppings.append(db_get_product(t))
        data['toppings'] = toppings
        res.append(data)
    return res


def get_to_price(products):
    res_sum = 0
    for p in products:
        res_sum += p['price']
    return float(res_sum)


@decorator_db
def get_orders_user(id_user,
                    date_start,
                    limit,
                    offset,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_order, date_create, code_order, ref_id_order_status, ref_id_coffee_shop, status
        FROM orders, order_status
        WHERE ref_id_user = {}
        AND date_create <= '{}'
        AND ref_id_order_status = id_order_status
        ORDER BY id_order DESC, ref_id_order_status ASC 
        LIMIT {} OFFSET {}
    '''.format(id_user,
               '{}-{}-{}'.format(date_start.year, date_start.month, date_start.day),
               int(limit),
               int(limit)*int(offset))
    # print(select)
    cur.execute(select)
    rows = cur.fetchall()
    data = [
        {
            'id_order': r[0],
            'date_create': str(r[1]),
            'code_order': r[2],
            'id_order_status': r[3],
            'order_status':r[5],
            'shop': get_information_shop(r[4],
                                         cur),
            'products': get_products_order(r[0],
                                           cur)
        }
        for r in rows
    ]
    for d in data:
        d['price'] = get_to_price(d['products'])
    return {
        'code': 1,
        'message': good_get_history,
        'data': {
            'orders': data
        }
    }


@decorator_db
def db_check_order(id_order,
                   id_user,
                   *args,
                   **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_order, ref_id_order_status
        FROM orders
        WHERE id_order = {}
        AND ref_id_user = {}
    '''.format(id_order,
               id_user)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[1] == 1:
            return {
                'code': 1,
                'message': '',
                'data': 0
            }
        return {
            'code': -2,
            'message': '',
            'data': 0
        }  # status is not new
    return {
        'code': -1,
        'message': '',
        'data': 0
    }  # order is not order user


@decorator_db
def remove_products_from_order(id_order,
                               *args,
                               **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_order_product, quantity, other_information
        FROM order_product
        WHERE ref_id_order = {}
    '''.format(id_order)
    cur.execute(select)
    for r in cur.fetchall():
        try:
            select_rem = '''
                UPDATE product
                SET quantity = quantity + {}
                WHERE id_product = {}    
            '''.format(r[2]['quantity'], r[2]['id_product'])
            cur.execute(select_rem)
            conn.commit()
        except:
            conn.rollback()
        for t in r[2]['toppings']:
            try:
                select_rem = '''
                    UPDATE product
                    SET quantity = quantity + {}
                    WHERE id_product = {}    
                    '''.format(r[2]['quantity'], t)
                cur.execute(select_rem)
                conn.commit()
            except:
                conn.rollback()
    select = '''
        DELETE FROM order_product WHERE ref_id_order = {}
    '''.format(id_order)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_remove_products_from_order,
        'data': 0
    }


@decorator_db
def db_update_order(id_order,
                    id_shop,
                    id_user,
                    products,
                    time,
                    comments,
                    *args,
                    **kwargs):
    def generate_code():
        return ''.join([str(random.randint(0, 9)) for _ in range(0, 6)])

    res = db_check_order(id_order,
                         id_user)
    if res['code'] > 0:
        res = remove_products_from_order(id_order)
        if res['code'] == 1:
            conn = kwargs['conn']
            cur = conn.cursor()
            is_valid_data, list_products = check_product_in_one_shop(products,
                                                                     id_shop,
                                                                     cur)
            if is_valid_data:
                code = generate_code()
                update = '''
                    UPDATE orders
                    SET reprieve_time = {},
                    comment = '{}',
                    code_order= '{}'
                    WHERE id_order = {}
                '''.format(time,
                           comments,
                           code,
                           id_order)
                cur.execute(update)
                conn.commit()
                return insert_products_to_order(id_order,
                                                products,
                                                list_products,
                                                id_user,
                                                id_shop,
                                                code,
                                                cur,
                                                conn)

            return {
                'code': 1,
                'message': error_not_correct_data,
                'data': {}
            }

        else:
            return res

    else:
        if res['code'] == -1:
            return {
                'code': 1,
                'message': error_update_not_your_order,
                'data': {}
            }
        else:
            return {
                'code': 1,
                'message': error_update_not_new_order,
                'data': {}
            }


@decorator_db
def db_reject_order(id_order,
                    id_user,
                    *args,
                    **kwargs):
    res = db_check_order(id_order,
                         id_user)
    if res['code'] > 0:
        select = '''
            UPDATE orders
            SET ref_id_order_status = 4
            WHERE ref_id_user = {}
            AND id_order = {}
        '''.format(id_user, id_order)
        conn = kwargs['conn']
        cur = conn.cursor()
        cur.execute(select)
        conn.commit()
        return {
            'code': 1,
            'message': good_reject_order,
            'data': {}
        }
    else:
        if res['code'] == -1:
            return {
                'code': 1,
                'message': error_update_not_your_order,
                'data': {}
            }
        else:
            return {
                'code': 1,
                'message': error_update_not_new_order,
                'data': {}
            }


@decorator_db
def db_get_favorite_shops(id_user,
                          page,
                          limit,
                          *args,
                          **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_coffee_shop, ref_id_coffee_shop_network,title,path_img,address,ST_x(ST_Transform( location, 4326)),
          ST_y(ST_Transform( location, 4326))
        FROM favorite_shop, coffee_shop, city
        WHERE ref_id_user = {}
        AND ref_id_shop = id_coffee_shop
        AND is_activate = TRUE 
        AND ref_id_city = id_city
        ORDER BY id_coffee_shop ASC 
        limit {} OFFSET {}
    '''.format(id_user,
               limit,
               int(page) * int(limit))
    cur.execute(select)

    res = [
        {
            'id_coffee_shop': r[0],
            'data_network': get_data_network(r[1]),
            'title': r[2],
            'path_img': r[3],
            'address': r[4],
            'location':{
                'lat': r[5],
                'lng':r[6]
            },
            'distance': 0
        } for r in cur.fetchall() if not bool(get_data_network(r[1])['is_ban'])]
    return {
        'code': 1,
        'message': good_get_favorite_shops,
        'data': {
            'favorite_shops': res
        }
    }


@decorator_db
def db_get_favorite_products(id_user,
                             page,
                             limit,
                             *args,
                             **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
            SELECT pr.id_product, pr.ref_id_category,pr.title_product, pr.price, pr.path_img, c.title_category
            FROM product pr, category c, favorite_product fp 
            WHERE fp.ref_id_user = {}
            AND fp.ref_id_product = pr.id_product
            AND c.id_category = pr.ref_id_category
            ORDER BY pr.id_product 
            limit {} OFFSET {}
        '''.format(id_user,
                   limit,
                   int(page) * int(limit))
    cur.execute(select)
    rows = cur.fetchall()
    result = {}
    for r in rows:
        try:
            arr = result[r[5]]
            arr.append({
                'id_product': r[0],
                'title_product': r[2],
                'price': r[3],
                'path_img': r[4]
            })
            result[r[5]] = arr
        except KeyError:
            result[r[5]] = [
                {
                    'id_product': r[0],
                    'title_product': r[2],
                    'price': float(r[3]),
                    'path_img': r[4]
                }
            ]
    res = [
        {
            'name_category': r,
            'products': result[r]
        } for r in result.keys()
    ]

    return {
        'code': 1,
        'message': good_get_favorite_products,
        'data': {
            'categories': res
        }
    }


@decorator_db
def db_to_favorite_product(id_user,
                           id_product,
                           to_favorite,
                           *args,
                           **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    try:
        if to_favorite:
            select = '''
                INSERT INTO favorite_product (ref_id_user, ref_id_product) VALUES ({},{})
            '''.format(id_user, id_product)
            message = good_add_to_favorite_products
        else:
            select = '''
                DELETE from favorite_product WHERE ref_id_user = {} AND ref_id_product = {}
            '''.format(id_user, id_product)
            message = good_remove_from_favorite_products
        cur.execute(select)
        conn.commit()
    except:
        message = error_work_with_favorite_products
    return {
        'code': 1,
        'message': message,
        'data': {}
    }


@decorator_db
def db_to_favorite_shop(id_user,
                           id_shop,
                           to_favorite,
                           *args,
                           **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    try:
        if to_favorite:
            select = '''
                INSERT INTO favorite_shop (ref_id_user, ref_id_shop) VALUES ({},{})
            '''.format(id_user, id_shop)
            message = good_add_to_favorite_products
        else:
            select = '''
                DELETE from favorite_shop WHERE ref_id_user = {} AND ref_id_shop = {}
            '''.format(id_user, id_shop)
            message = good_remove_from_favorite_products
        cur.execute(select)
        conn.commit()
    except:
        message = error_work_with_favorite_products
    return {
        'code': 1,
        'message': message,
        'data': {}
    }


