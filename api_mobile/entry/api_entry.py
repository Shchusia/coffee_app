from flask import request, \
    jsonify
from api_mobile.__init__ import app
from api_mobile.urls import api_registration,\
    api_authorisation
from api_mobile.answer_server import error_arguments,\
    good_sign_out,\
    error_not_valid_token
from api_mobile.entry.db_entry import db_registration,\
    db_sign_in,\
    token_to_data
from api_mobile.redis.redb import set_client_multi,\
    sign_out_multi
from api_mobile.urls_barista import api_entry_barista
import traceback


def registration():
    try:
        full_name = request.json['full_name']
        password = request.json['password']
        try:
            mail = request.json['email']
        except KeyError:
            mail = None
        try:
            phone = request.json['phone']
        except KeyError:
            phone = None
        if mail or phone:
            res = db_registration(full_name,
                                  password,
                                  mail,
                                  phone)
            if res['message']['code'] > 0:
                token = res['token']
                del res['token']
                answer = jsonify(res)
                answer.headers.extend({
                    'Token': token
                })
                set_client_multi(res['data']['user']['id_user'],
                                 token,
                                 res['data']['user']['role'])
                return answer
            return jsonify(res)
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })

    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })


def authorisation():
    try:
        password = request.json['password']
        try:
            mail = request.json['email']
        except KeyError:
            mail = None
        try:
            phone = request.json['phone']
        except KeyError:
            phone = None
        if mail or phone:
            res = db_sign_in(mail,
                             phone,
                             password)
            if res['message']['code'] > 0:
                token = res['token']
                del res['token']
                answer = jsonify(res)
                answer.headers.extend({
                    'Token': token
                })
                set_client_multi(res['data']['user']['id_user'],
                                 token,
                                 res['data']['user']['role'])
                return answer
            return jsonify(res)
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 1,
            'message': error_arguments,
            'data': {}
        })


def sign_out():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if sign_out_multi(data['id'],
                              token,
                              data['role']):
                return jsonify({
                    'code': 1,
                    'message': good_sign_out,
                    'data': {}
                })
            else:
                return jsonify({
                    'code': -1,
                    'message': error_not_valid_token,
                    'data': {}
                })
        return jsonify({
            'code': -1,
            'message': error_not_valid_token,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })


app.add_url_rule(api_registration, 'registration', registration, methods=['POST'])
app.add_url_rule(api_authorisation, 'authorisation', authorisation, methods=['POST'])



