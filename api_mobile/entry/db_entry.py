from api_mobile.postgres import decorator_db
from api_mobile.answer_server import error_unique_data,\
    good_registration,\
    error_authorisation,\
    good_sign_in
from config_app import role_client,\
    secret_word,\
    algorithm_encode,\
    role_worker_network
import jwt


def data_to_token(data):
    '''
    формирует токен по данным пользователя json
    :param data: данные пользователя имя пароль id
    :return: возвращает токен для пользователя
    '''

    return jwt.encode(data, secret_word, algorithm=algorithm_encode).decode()


def token_to_data(token):
    '''
    преобразовывает токен в данные пользователя
    :param token: токен который выдавался пользователю
    :return:json с данными пользователя
    '''
    try:
        return True, jwt.decode(token, secret_word, algorithms=[algorithm_encode])
    except jwt.exceptions.DecodeError:
        return False, {}


@decorator_db
def db_registration(full_name,
                    password,
                    mail,
                    phone,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    if phone and mail:
        select_find_mail_or_login = '''
          SELECT *
          FROM users
          WHERE mail = '{}' OR phone = '{}'
    '''.format(mail, phone)
        select_registration = '''
                    INSERT INTO users (full_name, phone, mail, password_hash) 
                    VALUES('{}', '{}', '{}', '{}') returning id_user
                '''.format(full_name,
                           phone,
                           mail,
                           password)
    elif phone:
        select_find_mail_or_login = '''
                  SELECT *
                  FROM users
                  WHERE phone = '{}'
            '''.format(mail, phone)
        select_registration = '''
            INSERT INTO users (full_name, phone, password_hash) 
            VALUES('{}', '{}', '{}') returning id_user
        '''.format(full_name,
                   phone,
                   password)
    else:
        select_find_mail_or_login = '''
                  SELECT *
                  FROM users
                  WHERE mail = '{}'
            '''.format(mail, phone)
        select_registration = '''
                    INSERT INTO users (full_name, mail, password_hash) 
                    VALUES('{}', '{}', '{}') returning id_user
                '''.format(full_name,
                           mail,
                           password)
    cur.execute(select_find_mail_or_login)
    rows = cur.fetchall()
    if len(rows) == 0:
        cur.execute(select_registration)
        id_user = cur.fetchone()[0]
        conn.commit()
        data_user = get_user(id_user)
        token = data_to_token(data_user)
        return {
            'code': 1,
            'message': good_registration,
            'data': {
                'user': data_user},
            'token': token
        }
    else:
        # print('asd')
        return {
            'code': 1,
            'message': error_unique_data,
            'data': {}
        }


@decorator_db
def get_user(id_user,
             *args,
             **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_get_user = '''
        SELECT id_user, full_name, phone, mail, is_ban
        FROM users
        WHERE id_user = {}
    '''.format(id_user)
    cur.execute(select_get_user)
    row = cur.fetchone()
    return {
        'id_user': row[0],
        'full_name': row[1],
        'phone': row[2],
        'mail': row[3],
        'is_ban': row[4],
        'role': role_client
    }


@decorator_db
def db_sign_in(email,
               phone,
               password,
               *args,
               **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_user, phone,mail,password_hash
        FROM users
        WHERE (mail = '{}' or phone = '{}')
        AND password_hash = '{}'
    '''.format(email, phone, password)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[1] == phone or row[2] == email:
            if row[3] == password:
                data_user = get_user(row[0])
                token = data_to_token(data_user)
                return {
                    'code': 1,
                    'message': good_sign_in,
                    'data': {
                        'user': data_user},
                    'token': token
                }
    return {
        'code': 1,
        'message': error_authorisation,
        'data': {}
    }


