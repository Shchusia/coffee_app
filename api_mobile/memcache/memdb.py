from config_app import cache


def set_data_network_shop(id_network,
                          data_network):
    try:
        data_networks = cache.get('network')[int(id_network)]
    except TypeError:
        data_networks = {}
    except KeyError:
        data_networks = {}
    data_networks[int(id_network)] = data_network

    cache.set('network', data_networks)


def get_data_network_shop(id_network):
    try:
        return cache.get('network')[int(id_network)]
    except :
        return None


def set_data_shop(id_shop,
                  data):
    try:
        data_networks = cache.get('shop')[int(id_shop)]
    except TypeError:
        data_networks = {}
    except KeyError:
        data_networks = {}
    data_networks[int(id_shop)] = data

    cache.set('shop', data_networks)


def get_data_shop(id_shop):
    try:
        return cache.get('shop')[int(id_shop)]
    except:
        return None