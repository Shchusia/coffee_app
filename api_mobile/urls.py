from config_app import domain_api_mobile


api_registration = domain_api_mobile + '/registration'
api_authorisation = domain_api_mobile + '/authorisation'
api_logout = domain_api_mobile + '/logout'

api_get_coffee_shops_by_coordinates = domain_api_mobile + '/coffee_shops/location'
api_get_coffee_shops_by_city = domain_api_mobile + '/coffee_shops/city'
api_get_city_list = domain_api_mobile + '/list/city'

api_get_products = domain_api_mobile + '/shop/<id_shop>/products'
api_get_product = domain_api_mobile + '/product/<id_product>'
api_create_order = domain_api_mobile + '/order'
api_list_orders = domain_api_mobile + '/orders'
api_update_order = domain_api_mobile + '/order/update'
api_order_reject = domain_api_mobile + '/order/reject'

api_get_favorite_products = domain_api_mobile + '/favorite/products'
api_get_favorite_shops = domain_api_mobile + '/favorite/shops'
api_favorite_update_shop = domain_api_mobile + '/favorite/shops/update'
api_favorite_update_product = domain_api_mobile + '/favorite/product/update'

api_get_user_info = domain_api_mobile + '/user'
api_update_user = domain_api_mobile + '/user/update'
