from config_app import domain_api_mobile_worker

api_entry_barista = domain_api_mobile_worker + '/entry'
api_get_products_barista = domain_api_mobile_worker + '/products'
api_create_order_barista = domain_api_mobile_worker + '/order'
api_get_list_order = domain_api_mobile_worker + '/orders'
api_update_status_worker = domain_api_mobile_worker + '/order/status/update'

api_get_list_product_to_swap = domain_api_mobile_worker + '/product/to/swap'
api_get_swap_products = domain_api_mobile_worker + '/swap'

api_get_order = domain_api_mobile_worker + '/order/<id_order>'

api_get_budget_day = domain_api_mobile_worker + '/budget'
api_move_barista = domain_api_mobile_worker + '/move/barista'
api_withdraw_money = domain_api_mobile_worker + '/withdraw/money'
api_get_list_shops_network = domain_api_mobile_worker + '/shops/network'

