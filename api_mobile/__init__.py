from flask import Flask,render_template,request,redirect,url_for,session,jsonify,blueprints
import traceback
import logging
from logging.handlers import RotatingFileHandler
from celery import Celery
from config_app import path_to_celery
from api_admin.entry.db_entry import token_to_data
from api_admin.redis.redb import check_valid_data_multi
from api_admin.server_answers import not_valid_token,\
    arguments_error
app = Flask(__name__)
handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)
async_mode = None


app.config['CELERY_BROKER_URL'] = path_to_celery
app.config['CELERY_RESULT_BACKEND'] = path_to_celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


def check_valid_data_user(fn):
    def wrap(*args, **kwargs):
        try:
            # from pprint import pprint
            # pprint(request.headers)
            token = request.headers['Token']
            is_good, data = token_to_data(token)
            # print(data)
            if is_good:
                if check_valid_data_multi(data['id_user'],
                                          token,
                                          data['role']):
                    return fn(*args,
                              **kwargs)
                else:
                    return jsonify({
                        'code': -1,
                        'message': not_valid_token,
                        'data': {}
                    })
            return jsonify({
                'code': -1,
                'message': not_valid_token,
                'data': {}
            })

        except KeyError:
            traceback.print_exc()
            # print('fuck')
            return jsonify({
                'code': 0,
                'message': arguments_error,
                'data': {}
            })
    return wrap


def check_valid_data_barista(fn):
    def wrap(*args, **kwargs):
        try:
            # from pprint import pprint
            # pprint(request.headers)
            token = request.headers['Token']
            is_good, data = token_to_data(token)
            print(data)
            if is_good:
                if check_valid_data_multi(data['id_worker'],
                                          token,
                                          data['role']):
                    return fn(*args,
                              **kwargs)
                else:
                    return jsonify({
                        'code': -1,
                        'message': not_valid_token,
                        'data': {}
                    })
            return jsonify({
                'code': -1,
                'message': not_valid_token,
                'data': {}
            })

        except KeyError:
            traceback.print_exc()
            # print('fuck')
            return jsonify({
                'code': 0,
                'message': arguments_error,
                'data': {}
            })
    return wrap


from api_mobile.entry import api_entry
from api_mobile.shops import api_shops
from api_mobile.user import api_user
from api_mobile.barista import api_barista
