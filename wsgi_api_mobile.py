import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


from api_mobile_barista.__init__ import init_app
import asyncio
from aiohttp import web


loop_ = asyncio.get_event_loop()
app_ = loop_.run_until_complete(init_app(loop_))
web.run_app(app_, port=5010)
