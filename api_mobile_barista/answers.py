# !__ good
good_entry = {
    'title': 'успех',
    'body': 'успешный вход',
    'code': 1
}

good_restore_password = {
    'title': 'успех',
    'body': 'успешно изменен пароль',
    'code': 2
}

good_logout = {
    'title': 'успех',
    'body': 'успешный выход',
    'code': 3
}

good_get_shops = {
    'title': 'успех',
    'body': 'успешно получили объекты',
    'code': 4
}

good_move_to_shop = {
    'title': '',
    'body': '',
    'code': 5
}

good_get_menu = {
    'title': '',
    'body': '',
    'code': 6
}

# ! error

error_valid_data_for_entry = {
    'title': 'ошибка входа',
    'body': 'неверный логин и(или) пароль',
    'code': -1
}

error_limit_send_sms = {
    'title': 'ошибка входа',
    'body': 'превышен лимит сообщений в день',
    'code': -2
}

error_time_send_sms = {
    'title': 'ошибка входа',
    'body': 'время до следующего sms еще не вышло',
    'code': -3
}

error_not_exist_user_with_data = {
    'title': 'ошибка входа',
    'body': 'время до следующего sms еще не вышло',
    'code': -4
}

error_not_shop_in_your_network = {
    'title': 'ошибка входа',
    'body': 'время до следующего sms еще не вышло',
    'code': -5
}

error_cod_is_ald = {
    'title': 'ошибка входа',
    'body': 'время до следующего sms еще не вышло',
    'code': -6
}

error_not_exist_your_shop = {
    'title': 'ошибка входа',
    'body': 'время до следующего sms еще не вышло',
    'code': -7
}


error_unknown = {
    'title': 'ошибка',
    'body': 'неизвестная',
    'code': -30
}

error_in_select = {
    'title': 'ошибка в запросе',
    'body': 'ошибка в запросе к базе данных',
    'code': -31
}

error_in_connect = {
    'title': 'ошибка при коннекте',
    'body': 'ошибка попытке подключиться к базе данных',
    'code': -32
}

error_arguments = {
    'title': 'ошибка',
    'body': 'недостаточно вргументов для выполнения запроса',
    'code': -33
}

error_not_valid_token = {
    'title': 'ошибка токена',
    'body': 'не валидный токен',
    'code': -34,
}

