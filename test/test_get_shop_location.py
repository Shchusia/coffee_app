from config_app import domain_api_mobile
import requests
from pprint import pprint
from api_mobile.urls import api_get_coffee_shops_by_coordinates

base_url = 'http://127.0.0.1:6967'

data = {
    'lat': 50.027352,
    'lng': 36.224943,
    'page': 0,
    'limit': 2
}
res = requests.post(base_url + api_get_coffee_shops_by_coordinates,json=data)

if res.status_code == 200:
    print(res.json())

