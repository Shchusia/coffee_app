token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmdWxsX25hbWUiOiJERW5pcyIsImlkX3Nob3AiOjUsImxvZ2luIjoiZGVuaXNrYSIsInBob25lIjoiIiwiaWRfdXNlciI6MywiaWRfbmV0d29yayI6Nn0.rYSs-SQMarWATLaV8_blq6C1gTOAYIjezkrz_lHPNxg'

import requests
domain_api_mobile_worker = '/coffee/app/mobile/api/v1/barista'

base_url = 'http://127.0.0.1:5050'
api_get_shops = base_url + domain_api_mobile_worker + '/get_shops'

res = requests.get(api_get_shops, headers={'Token': token})
print(res.status_code)
print(res.json())



# {'code': 1, 'data': {'shops': [{'time_end': '22:00:00', 'address': 'проспект Науки, 29, Харків, Харківська область, Україна, 61000', 'title': 'главная кофейня', 'time_start': '10:00:00', 'path_img': '/coffee_app/files/daefad3bbd8e3fe9b9dbbf36038d7605c5fe1623.jpeg', 'working_day': [True, True, True, True, True, True, True], 'description': '', 'id_shop': 5, 'latlon': {'latitude': 50.028254906412116, 'longitude': 36.221580505371094}}]}, 'message': {'body': 'успешно получили объекты', 'code': 4, 'title': 'успех'}}