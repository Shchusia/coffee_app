from config_app import domain_api_mobile
import requests
from pprint import pprint

api_get_products = domain_api_mobile + '/shop/7/products'
base_url = 'http://127.0.0.1:6967'

res = requests.get(base_url + api_get_products,)
if res.status_code == 200:
    pprint(res.json())
