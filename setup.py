from api_mobile_barista.__init__ import app
from aiohttp import web


if __name__ == '__main__':
    web.run_app(app, port=5050)
