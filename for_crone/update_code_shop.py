#EDITOR=nano crontab -e
#0 0 * * * python3 '/home/denis/PycharmProjects/production projects/medical_messenger/find_for_congratulation.py'

import psycopg2
from config_app import str_connect_to_db
import traceback
from api_admin.server_answers import error_in_select,\
    error_in_connect
import random


def generate_code():
    return ''.join([str(random.randint(0, 9)) for _ in range(0, 6)])


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except:
        print("I am unable to connect to the database")
    return conn


def function_update():
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        select = '''
            SELECT id_coffee_shop
            FROM coffee_shop
        '''
        cur.execute(select)
        rows = cur.fetchall()
        for r in rows:
            select = ''' 
                UPDATE coffee_shop
                SET code_for_entry = '{}'
                WHERE id_coffee_shop = {}
            '''.format(generate_code(), r[0])
            try:
                cur.execute(select)
                conn.commit()
            except:
                conn.rollback()
    conn.close()
