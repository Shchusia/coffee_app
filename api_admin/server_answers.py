good_sign_in = {
    'title': 'успех',
    'body': 'успешный вход',
    'code': 1,
}

valid_token = {
    'title': 'успех',
    'body': 'ваши данные валидны',
    'code': 2
}

good_sign_out = {
    'title': 'успех',
    'body': 'успешный выход',
    'code': 3,
}

good_upload_file = {
    'title': 'успех',
    'body': 'файл успешно загружен',
    'code': 4,
}

create_coffee_network = {
    'title': 'успех',
    'body': 'сеть создана',
    'code': 5,
}

good_select = {
    'title': 'успех',
    'body': 'запрос успешно выполнен',
    'code': 6,
}

# ___________________ #

arguments_error = {
    'title': 'ошибка',
    'body': 'нет необходимых аргументов для выполнения запроса',
    'code': -1
}

sign_in_error = {
    'title': 'ошибка входа',
    'body': 'неверный логин или пароль',
    'code': -2
}

login_exist = {
    'title': 'ошибка регистрации',
    'body': 'такй логин уже существует в системе',
    'code': -3
}

error_type_category = {
    'title': 'ошибка',
    'body': 'ошибка, нет такой категории',
    'code': -4
}

error_coffee_shop = {
    'title': 'ошибка',
    'body': 'данное заведение вам не принадлежит',
    'code': -5
}

error_in_select = {
    'title': 'ошибка в запросе',
    'body': 'ошибка в запросе к базе данных',
    'code': -31
}

error_in_connect = {
    'title': 'ошибка при коннекте',
    'body': 'ошибка попытке подключиться к базе данных',
    'code': -31
}

not_valid_token = {
    'title': 'ошибка токена',
    'body': 'не валидный токен',
    'code': -30,
}

error_unknown = {
    'title': 'ошибище',
    'body': 'природы',
    'code': -32
}
