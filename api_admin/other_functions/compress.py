from PIL import Image
import os


def comppress_photo(path_to_file):
    img = Image.open(path_to_file)
    if img.size[1] > 2560:
        comp_image_height(path_to_file, 2560)
    img = Image.open(path_to_file)
    if img.size[0] > 1280:
        comp_image_width(path_to_file, 1280)
    img = Image.open(path_to_file)
    new_file = '.'.join(path_to_file.split('.')[:-1])
    os.remove(path_to_file)
    img.save(new_file+'.jpeg')
    # return new_file+'.jpeg'


def comp_image_height(path_to_file, height=2560):
    try:
        img = Image.open(path_to_file)
        ratio = (height / float(img.size[1]))
        width = int((float(img.size[0]) * float(ratio)))
        img = img.resize((width, height), Image.ANTIALIAS)
        img.save(path_to_file)
    except:
        # traceback.print_exc()
        pass


def comp_image_width(path_to_file, width=1280):
    try:
        img = Image.open(path_to_file)
        ratio = (width / float(img.size[0]))
        height = int((float(img.size[1]) * float(ratio)))
        img = img.resize((width, height), Image.ANTIALIAS)
        img.save(path_to_file)
    except:
        pass
